import logo from './logo.svg';
import './App.css';
import HomePage from './Components/HomePage';
import Login from './Components/Login';
import Register from './Components/Register';
import Page404 from './Components/Page404';
import {Provider} from 'react-redux'
import store from './strore'

import {
  BrowserRouter as Router,
  Route,
  Routes,
  Link,
}
  from 'react-router-dom'

function App() {
  return (
    <Provider store = {store}>
    <Router>
      <Routes>
        <Route exact path="/" element={<HomePage />} />
        <Route exact path="/login" element={<Login />} />
        <Route exact path="/register" element={<Register />} />
        <Route exact path="/*" element={<Page404 />} />
      </Routes>
    </Router>
    </Provider>
  );
}

export default App;
